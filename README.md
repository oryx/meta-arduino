# meta-arduino

## About

This OpenEmbedded BSP layer supports the Arduino Yún board and is based on
previous work from OpenWRT. Future support is planned for the Arduino Industrial
101 board and the Arduino Yún Shield.

Maintainers:
* Beth 'pidge' Flanagan <pidge@toganlabs.com>
* Paul Barker <pbarker@toganlabs.com>

Primary repository: https://gitlab.com/toganlabs/meta-arduino

Please send any questions and patches to the yocto@yoctoproject.org mailing list
with "[meta-arduino]" in the subject.

We are not associated with Arduino LLC in any way. This is solely a community 
supported alternative to the LininoOS OpenWRT based solution. Support requests
and bug reports should go directly to the maintainers and not the nice folks
at Arduino.

## Supported boards and images

MACHINEs:
* arduino-yun: Arduno Yún board

Use core-image-minimal from openembedded-core for bring-up and initial testing.

## Dependencies

This layer depends on just the openembedded-core layer.

    URI: git://git.openembedded.org/openembedded-core
    branch: master
    revision: HEAD

## Arduino Yún Bring-up

A newly purchased Yún will likely be flashed with an old version of the
OpenWRT or Linino firmware. The following instructions can be used to get
started with an image built using OpenEmbedded. These instructions place the
rootfs image on an SD card due to the small size of the onboard flash on the
Yún. They also obtain the kernel over TFTP at every boot so as to avoid having
to write to flash at all. This avoids risk of overwriting the original firmware
image in flash incase it is needed at a later date.

NOTE: Follow these instructions carefully and at your own risk!

1) Install the Arduino IDE. On Debian, this is as simple as issuing `sudo apt
install arduino`. On other operating systems, please see the [Arduino IDE
download page].

2) Set up a TFTP server accessible on the same network as the Yún. Make the
uImage file produced by the OpenEmbedded build available on this server.

3) Flash the ext2 rootfs image produced by OpenEmbedded to the first partition
on a microSD card and place this card into the Yún.

4) Upload the [Yún serial terminal script] to the ATmega32U4 module on the
device. You should now be able to connect to the Yún via the USB serial adaptor
(which shows up as /dev/ttyACM0 or similar for me). If you reset the device via
the "YÚN RST" button, you should see boot messages from U-boot.

5) Interrupt the boot process by typing "lin" at the appropriate time. This
should drop you to a u-boot prompt.

6) By default the Yún serial terminal operates at 250000 baud but this is not
supported by Busybox as used in the images produced by this layer. The baud rate
needs to be changed to 115200 baud before continuing. Issue the following
command at the u-boot prompt:

    setenv baudrate 115200

Now, type '~1' to set the baudrate to 115200 in the Yún serial terminal
application running on the ATmega32U4 processor. If this setting is saved, '~1'
will have to be issued whenever the ATmega32U4 processor is rebooted to set the
baudrate back to 115200.

7) Issue the following commands to prepare to boot over TFTP. Replace the ipaddr
value with a valid address for the Yún to use and the serverip value with the
address of the TFTP server.

    setenv ipaddr 192.168.1.2
    setenv serverip 192.168.1.3
    setenv board Yun
    setenv addrootfs 'setenv bootargs ${bootargs} root=/dev/sda1 rootwait rw noinitrd'
    setenv bootcmd 'tftp 0x80200000 uImage && run addboard && run addtty && run addparts && run addrootfs && bootm 0x80200000'

8) Optionally, issue 'saveenv' to make these changes permanent.

9) Issue 'boot' to start the TFTP boot process. You should see the kernel
downloaded over TFTP and then booted. Once Linux has booted, you should be
greeted with an `arduino-yun login:` prompt.

For further instructions, see the [Reflashing U-boot/Kernel/Rootfs on Arduino
Yún] page.

[Arduino IDE download page]:https://www.arduino.cc/en/Main/Software
[Yún serial terminal script]:https://www.arduino.cc/en/Tutorial/YunSerialTerminal
[Reflashing U-boot/Kernel/Rootfs on Arduino Yún]:https://www.arduino.cc/en/Tutorial/YunUBootReflash
