SUMMARY = "Linux kernel"
SECTION = "kernel"
LICENSE = "GPLv2"
COMPATIBLE_MACHINE = "arduino-yun"

LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

inherit kernel

LINUX_VERSION = "4.9.45"
PV = "${LINUX_VERSION}"

SRC_URI = "https://downloads.toganlabs.com/${PN}/tiyun-${LINUX_VERSION}/${PN}-${LINUX_VERSION}.tar.xz"
SRC_URI[md5sum] = "0ba29639cc37ab5a709a34900b3a3545"
SRC_URI[sha256sum] = "5a3fda88217cf71138bef84f1f5705be02baf327c2a558e60745a9ff677a39b2"

KERNEL_IMAGETYPE = "uImage"

S = "${WORKDIR}/${PN}-${PV}"

do_configure_prepend() {
    oe_runmake -C "${S}" O="${B}" arduino_yun_defconfig
}
